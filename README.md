# Data Engineering Exercises

## Weather history

1. Load the data into memory.
2. Process/transform the data (if necessary) into a suitable format.
3. Get the number of snow days for each year.
4. Get the lowest minimum and highest maximum temperatures for each month.
5. Get the average maximum temperature for each month.
6. Get the last record (by date) for each month and write the data set out in CSV format with
   strings quoted with double quotes.

## Olympic medals

1. Load the data into memory.
2. Process/transform the data (if necessary) into a suitable format.
3. For each event that has a distance, standardize the distance in miles.
4. Write the data set out in pipe delimited format with all fields quoted and a header row in all caps.

## Movies

1. Load the 2 data sets into memory.
2. Process/transform the data (if necessary) into a suitable format.
3. Join the data sets via genre id and output the following fields in a CSV format:
    * movie_id
    * original_title
    * original_language
    * genre_id
    * genre_name
    * release_date
    * release_day_of_week
    * popularity
4. Sort the data set by decreasing popularity

Thanks to TMDb (https://www.themoviedb.org/) for partial data sets.
